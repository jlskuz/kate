# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# obsoleteman <tulliana@gmail.com>, 2008-2009.
# Volkan Gezer <volkangezer@gmail.com>, 2013, 2014, 2019, 2022.
# Kaan Ozdincer <kaanozdincer@gmail.com>, 2014.
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: applications-kde4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 00:51+0000\n"
"PO-Revision-Date: 2023-04-04 12:13+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "Bir kabuğa veya uçbirim öykünücüye erişmek için yeterli hakkınız yok"

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:664
#, kde-format
msgid "Terminal"
msgstr "Uçbirim"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "Uçbirime &Veriyolu Yap"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "Uçbirimi Geçerli Belge ile &Eşzamanla"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Mevcut Belgeyi Çalıştır"

#: kateconsole.cpp:156 kateconsole.cpp:513
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Uçbirim Panelini &Göster"

#: kateconsole.cpp:162
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "Uçbirim Paneline &Odaklan"

#: kateconsole.cpp:307
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""
"Konsole kurulu değil. Uçbirimi kullanabilmek için lütfen Konsole "
"uygulamasını kurun."

#: kateconsole.cpp:388
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Gerçekten metni uçbirime veriyolu yapmak istiyor musunuz? Bu işlem içerilen "
"birçok komutu sizin kullanıcı haklarınızla çalıştıracak."

#: kateconsole.cpp:389
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Uçbirime veriyolu yapılsın mı?"

#: kateconsole.cpp:390
#, kde-format
msgid "Pipe to Terminal"
msgstr "Uçbirime Veriyolu Yap"

#: kateconsole.cpp:418
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Üzgünüm, '%1' içine geçilemedi"

#: kateconsole.cpp:454
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Yerel bir dosya değil: '%1'"

#: kateconsole.cpp:487
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Gerçekten belgeyi çalıştırmak istiyor musunuz?\n"
"Bu işlem aşağıdaki komutu sizin -\n"
"kullanıcı haklarınızla çalıştıracak:\n"
"'%1'"

#: kateconsole.cpp:494
#, kde-format
msgid "Run in Terminal?"
msgstr "Uçbirimde çalıştırılsın mı?"

#: kateconsole.cpp:495
#, kde-format
msgid "Run"
msgstr "Çalıştır"

#: kateconsole.cpp:510
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "Uçbirim &Panelini Gizle"

#: kateconsole.cpp:521
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Uçbirim Panelindeki Odağı Kaldır"

#: kateconsole.cpp:522 kateconsole.cpp:523
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Uçbirim Paneline Odaklan"

#: kateconsole.cpp:597
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr "Uygun olduğunda, uçbirimi geçerli belge ile otomatik &olarak eşzamanla"

#: kateconsole.cpp:601 kateconsole.cpp:622
#, kde-format
msgid "Run in terminal"
msgstr "Uçbirimde çalıştır"

#: kateconsole.cpp:603
#, kde-format
msgid "&Remove extension"
msgstr "Uzantıyı &kaldır"

#: kateconsole.cpp:608
#, kde-format
msgid "Prefix:"
msgstr "Önek:"

#: kateconsole.cpp:616
#, kde-format
msgid "&Show warning next time"
msgstr "Bir sonraki &sefere uyarı göster"

#: kateconsole.cpp:618
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"'%1' bir sonraki çalıştırıldığında, bir uyarı penceresinin açıldığından emin "
"olun,incelenmek üzere uçbirime gönderilecek komutu gösterir."

#: kateconsole.cpp:629
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "&EDITOR çevre değişkenini 'kate -b' olarak ayarla"

#: kateconsole.cpp:632
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Önemli: Uçbirim uygulamasının sürdürülebilmesi için belgenin kapatılması "
"gerekir"

#: kateconsole.cpp:635
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "'Esc'ye basıldığında Konsole'yi gizle"

#: kateconsole.cpp:638
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Bu işlem Esc düğmesini kullanan uçbirim uygulamalarında sorunlar "
"çıkarabilir; örneğin Vim. Bu uygulamaları aşağıdaki girdiye ekleyin "
"(virgülle ayrılmış)"

#: kateconsole.cpp:669
#, kde-format
msgid "Terminal Settings"
msgstr "Uçbirim Ayarları"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Araçlar"
