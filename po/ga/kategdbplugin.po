# Irish translation of kategdbplugin
# Copyright (C) 2011 This_file_is_part_of_KDE
# This file is distributed under the same license as the kategdbplugin package.
# Kevin Scannell <kscanne@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kategdbplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-15 00:49+0000\n"
"PO-Revision-Date: 2011-12-28 12:28-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, fuzzy, kde-format
#| msgid "GDB command"
msgid "GDB command"
msgstr "Ordú GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Feidhmchlár logánta"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "TCP Cianda"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Port Srathach Cianda"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Óstríomhaire"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Port"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Bád"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, fuzzy, kde-format
#| msgid "Custom Startup Commands"
msgid "Custom Init Commands"
msgstr "Orduithe Saincheaptha Tosaithe"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Cuir sprioc nua leis"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Cóipeáil an sprioc"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Scrios an sprioc"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Comhad inrite:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Comhadlann Oibre:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr ""

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argóintí:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Coinnigh an fócas"

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Coinnigh an fócas ar líne na n-orduithe"

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Atreoraigh I/A"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Athdhírigh I/A an ríomhchláir dhífhabhtaithe ar chluaisín ar leith"

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Ardsocruithe"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Spriocanna"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Sprioc %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "Settings"
msgid "Settings File:"
msgstr "Socruithe"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr ""

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, fuzzy, kde-format
#| msgid "Debug"
msgid "Debugger"
msgstr "Dílochtú"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Logánta"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr ""

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Níorbh fhéidir an próiseas dífhabhtaithe a thosú"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** bhí gdb scortha mar is gnáth ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr ""

#: debugview.cpp:650
#, fuzzy, kde-format
#| msgid "Thread %1"
msgid "thread(s) running: %1"
msgstr "Snáithe %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr ""

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr ""

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr ""

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr ""

#: debugview.cpp:707
#, fuzzy, kde-format
#| msgid "Target %1"
msgid "Host: %1. Target: %1"
msgstr "Sprioc %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr ""

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr ""

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr ""

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr ""

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr ""

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr ""

#: debugview_dap.cpp:285
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "Breakpoint(s) reached:"
msgstr "Brisphointe"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr ""

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr ""

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr ""

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr ""

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr ""

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr ""

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr ""

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr ""

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr ""

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr ""

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr ""

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr ""

#: debugview_dap.cpp:479
#, fuzzy, kde-format
#| msgid "Thread %1"
msgid "thread %1"
msgstr "Snáithe %1"

#: debugview_dap.cpp:633
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "breakpoint set"
msgstr "Brisphointe"

#: debugview_dap.cpp:641
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "breakpoint cleared"
msgstr "Brisphointe"

#: debugview_dap.cpp:700
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "(%1) breakpoint"
msgstr "Brisphointe"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr ""

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr ""

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr ""

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr ""

#: debugview_dap.cpp:745
#, fuzzy, kde-format
#| msgid "Insert breakpoint"
msgid "conditional breakpoints"
msgstr "Ionsáigh brisphointe"

#: debugview_dap.cpp:746
#, fuzzy, kde-format
#| msgid "Insert breakpoint"
msgid "function breakpoints"
msgstr "Ionsáigh brisphointe"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr ""

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr ""

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr ""

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr ""

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr ""

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr ""

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr ""

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr ""

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr ""

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr ""

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr ""

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr ""

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr ""

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr ""

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr ""

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr ""

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr ""

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr ""

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr ""

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr ""

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr ""

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr ""

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr ""

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr ""

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr ""

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr ""

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr ""

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Siombail"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Luach"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr ""

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr ""

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr ""

#: plugin_kategdb.cpp:106
#, fuzzy, kde-format
#| msgid "Debug"
msgid "Kate Debug"
msgstr "Dílochtú"

#: plugin_kategdb.cpp:110
#, kde-format
msgid "Debug View"
msgstr "Amharc Dífhabhtaithe"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:343
#, kde-format
msgid "Debug"
msgstr "Dílochtú"

#: plugin_kategdb.cpp:113 plugin_kategdb.cpp:116
#, fuzzy, kde-format
#| msgid "Call Stack"
msgid "Locals and Stack"
msgstr "Cruach Glao"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Uimh"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Fráma"

#: plugin_kategdb.cpp:200
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "GDB Output"
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Aschur GDB"

#: plugin_kategdb.cpp:201
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Socruithe"

#: plugin_kategdb.cpp:243
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""

#: plugin_kategdb.cpp:268
#, kde-format
msgid "Start Debugging"
msgstr "Tosaigh Dífhabhtú"

#: plugin_kategdb.cpp:278
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Maraigh / Stop Dífhabhtú"

#: plugin_kategdb.cpp:285
#, kde-format
msgid "Continue"
msgstr "Lean ar aghaidh"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Scoránaigh Brisphointe / Briseadh"

#: plugin_kategdb.cpp:297
#, kde-format
msgid "Step In"
msgstr "Céimnigh Isteach"

#: plugin_kategdb.cpp:304
#, kde-format
msgid "Step Over"
msgstr "Céimnigh Thart"

#: plugin_kategdb.cpp:311
#, kde-format
msgid "Step Out"
msgstr "Céimnigh Amach"

#: plugin_kategdb.cpp:318 plugin_kategdb.cpp:350
#, kde-format
msgid "Run To Cursor"
msgstr "Rith go dtí an cúrsóir"

#: plugin_kategdb.cpp:325
#, kde-format
msgid "Restart Debugging"
msgstr "Atosaigh Dífhabhtú"

#: plugin_kategdb.cpp:333 plugin_kategdb.cpp:352
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Athraigh an PC"

#: plugin_kategdb.cpp:338
#, kde-format
msgid "Print Value"
msgstr "Priontáil Luach"

#: plugin_kategdb.cpp:347
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:349
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:431 plugin_kategdb.cpp:447
#, kde-format
msgid "Insert breakpoint"
msgstr "Ionsáigh brisphointe"

#: plugin_kategdb.cpp:445
#, kde-format
msgid "Remove breakpoint"
msgstr "Bain brisphointe"

#: plugin_kategdb.cpp:599 plugin_kategdb.cpp:613
#, kde-format
msgid "Execution point"
msgstr "Pointe rite"

#: plugin_kategdb.cpp:771
#, kde-format
msgid "Thread %1"
msgstr "Snáithe %1"

#: plugin_kategdb.cpp:871
#, kde-format
msgid "IO"
msgstr "I/A"

#: plugin_kategdb.cpp:956 plugin_kategdb.cpp:964
#, kde-format
msgid "Breakpoint"
msgstr "Brisphointe"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Dífhabhtaigh"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, fuzzy, kde-format
#| msgid "GDB Plugin"
msgid "Debug Plugin"
msgstr "Breiseán GDB"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Kevin Scannell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kscanne@gmail.com"

#~ msgid "GDB Integration"
#~ msgstr "Comhtháthú GDB"

#~ msgid "Kate GDB Integration"
#~ msgstr "Comhtháthú GDB in Kate"

#~ msgid "&Target:"
#~ msgstr "&Sprioc:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "Liosta &Argóintí:"

#~ msgid "Remove Argument List"
#~ msgstr "Bain Liosta Argóintí"

#~ msgid "Arg Lists"
#~ msgstr "Liostaí Argóintí"
